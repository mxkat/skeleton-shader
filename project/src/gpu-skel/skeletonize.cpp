#include <stdint.h>
#include <inttypes.h>
#include <string.h>

// Skeletonization algorithms
// Taken from E.R. Davies "Computer and Machine Vision" 4th ed book.
// ISBN 978-0123869081

// Returns the sigma value for determining whether a pixel should be removed
//
// Parameters:
//              data - 9 element array representing a pixel and its neighborhood
//
// Returns: Number of pixels in neighborhood that are 1 (0-9)
int sigma(uint8_t *data)
{
    int i;
    int sigma = 0;

    // Assume 9 pixels, 0 (pixel of interest) - 8
    // In image, 0 is center
    // 1 2 3
    // 8 0 4
    // 7 6 5

    for (i=1; i < 9; i++)
    {
        sigma += data[i];
    }

    return sigma;
}

// Calculates the chi value for determining whether a pixel should be removed
//
// Paramaters:
//              data - 9 element array representing a pixel and its neighborhood
//
// Returns: chi value (see book, chapter 9 for more details on how this is derived)
int chi(uint8_t *data)
{
    int chi;

    // Assume 9 pixels, 0 (pixel of interest) - 8
    // 1 2 3
    // 8 0 4
    // 7 6 5

    chi = (int) (data[1] != data[3]) +
          (int) (data[3] != data[5]) +
          (int) (data[5] != data[7]) +
          (int) (data[7] != data[1]) +

          2 * ( (int) ((data[2] > data[1]) && (data[2] > data[3])) ) +
          (int) ((data[4] > data[3]) && (data[4] > data[5])) +
          (int) ((data[6] > data[5]) && (data[6] > data[7])) +
          (int) ((data[8] > data[7]) && (data[8] > data[1]));

   return chi;
}



// Gets the neighborhood around a pixel of interest.
// Neighborhoods are arranged as:
//      0 (pixel of interest) - A8
//
//      1 2 3
//      8 0 4
//      7 6 5
//
// This arrangement is represented in a 1D array, where neighborhood[0] is
// pixel 0 (the center), neighborhood[1] is pixel 1 (top left), and so on.
//
// Parameters:
//              neighborhood - The 9 element array representing the 3x3 neighborhood
//              data         - The whole image being processed
//              pos          - Current (1D) position in the data array
//              width        - Width of the image
//              height       - Height of the image
//
// Returns: None. The pointer neighborhood is populated with values of the pixels
void getNeighborhood(uint8_t *neighborhood, uint8_t *data, int pos, uint16_t width, uint16_t height)
{
    int bottom = width * height - width; // Offset of bottom row
    int i;

    neighborhood[0] = data[pos];

    for (i=1; i < 9; i++)
    {
        // Mark all of the neighborhood as '2'. We will get any pixels
        // that are still marked this after we check the edges
        neighborhood[i] = 2;
    }

    if (pos < width)
    {
        // Pixel is on top row. Fill area outside of image with zero
        neighborhood[1] = 0;
        neighborhood[2] = 0;
        neighborhood[3] = 0;
    }

    if (pos % width == 0)
    {
        // Pixel is on left edge. Fill area outside of image with zero
        neighborhood[1] = 0;
        neighborhood[8] = 0;
        neighborhood[7] = 0;
    }

    if ((pos % width) == (width - 1))
    {
        // Pixel is on right edge.
        neighborhood[3] = 0;
        neighborhood[4] = 0;
        neighborhood[5] = 0;
    }

    if (pos >= bottom)
    {
        // Pixel is on bottom edge.
        neighborhood[5] = 0;
        neighborhood[6] = 0;
        neighborhood[7] = 0;
    }

    // Get remaining pixels
    for (i=1; i < 9; i++)
    {
        if (neighborhood[i] == 2)
        {
            switch (i)
            {
                case 1:
                    // Upper left pixel
                    neighborhood[i] = data[pos - 1 - width];
                    break;
                case 2:
                    // Upper middle pixel
                    neighborhood[i] = data[pos - width];
                    break;
                case 3:
                    // Upper right pixel
                    neighborhood[i] = data[pos + 1 - width];
                    break;
                case 4:
                    // Right pixel
                    neighborhood[i] = data[pos + 1];
                    break;
                case 5:
                    // Bottom right pixel
                    neighborhood[i] = data[pos + width + 1];
                    break;
                case 6:
                    // Bottom middle pixel
                    neighborhood[i] = data[pos + width];
                    break;
                case 7:
                    // Bottom left pixel
                    neighborhood[i] = data[pos + width - 1];
                    break;
                case 8:
                    // Left pixel
                    neighborhood[i] = data[pos - 1];
                    break;
            }
        }
    }
}

// Regularizes data back to usable values for OpenGL:
//      0x0 stays 0x0
//      0x1 becomes 0xFF
//
// Parameters:
//              data   - The entire image
//              width  - Width of the image
//              height - Height of the image
void regularize(uint8_t *data, uint16_t width, uint16_t height)
{
    int i;
    int area = width * height;

    for (i=0; i < area; i++)
    {
        if (data[i] == 1)
        {
            data[i] = 0xFF;
        }
    }
}

// Normalizes data for use with skeleton algorithms
//      0xFF becomes 0x1
//      All other values become 0x0
//
// Parameters:
//              data   - The entire image
//              width  - Width of the image
//              height - Height of the image
void normalize(uint8_t *data, uint16_t width, uint16_t height)
{
    int i;
    int area = width * height;

    for (i=0; i < area; i++)
    {
        if (data[i] == 0xFF)
        {
            data[i] = 1;
        }
        else
        {
            data[i] = 0;
        }
    }
}


// Determines whether a pixel should be removed. If so, it removes the pixel
//
// Parameters:
//              neighborhood - The 3x3 neighborhood of pixels, represented as a 1D array
//              data         - The entire image
//              done         - If the algorithm is finished. Any time a pixel
//                             is removed, the algorithm is not finished. If
//                             done remains true after an entire iteration of
//                             the skeleton algorithm, the algorithm is finished.
//                             This value is used to determine when to break out
//                             of the main algorithm loop
//
// Returns: 1 if a pixel was removed, 0 otherwise
int removePoint(uint8_t* neighborhood, uint8_t *data, bool &done)
{
    if (chi(neighborhood) == 2 && sigma(neighborhood) != 1)
    {
        *data = 0;
        done = false;
        return 1;
    }
    return 0;
}



// North-South-East-West skeletonization
//
// This is the improved version over Skeleton(). This function is parallelizable,
// and is ported to the compute shader for the GPU version
//
// The N-S-E-W skeletonization removes all of the top pixels (north) it can,
// then removes all the right pixels (east) it can, etc, until no more pixels
// can be removed, leaving a skeleton image.
//
// Parameters:
//              data   - 8 bit data, representing an image, where each byte is either 0x0 or 0xFF
//              width  - width of the image
//              height - height of the image
void Skeleton2(uint8_t *data, uint16_t width, uint16_t height)
{
    uint8_t neighborhood[9];
    bool done = true;
    int area = width * height;
    int i;
    int mode = 0; // 0 = north, 2 = south, 3 = east, 4 = west
    int iterations = 0;
    
    // We will create two extra copies of the memory. When
    uint8_t *imData;
    uint8_t *in;
    uint8_t *out;

    // Normalize data. 0xFF -> 1, all else = 0
    normalize(data, width, height);

    imData = new uint8_t[width * height];

    in = data;
    out = imData;

    do
    {
        iterations++;
        memcpy(imData, data, sizeof(uint8_t) * width * height);
        done = true;

        for (i=0; i < area; i++)
        {
            // Only consider cases where the center is 1
            if (in[i] == 1)
            {
                getNeighborhood(neighborhood, in, i, width, height);
            }
            else
            {
                continue;
            }

            switch (mode)
            {
                case 0:
                //north
                    if (neighborhood[2] == 0 && neighborhood[6] == 1)
                    {

                        removePoint(neighborhood, out + i, done);

                    }
                    break;
                case 1:
                //south
                    if (neighborhood[2] == 1 && neighborhood[6] == 0)
                    {
                        removePoint(neighborhood, out + i, done);
                    }
                    break;
                case 2:
                //east
                    if (neighborhood[4] == 0 && neighborhood[8] == 1)
                    {
                        removePoint(neighborhood, out + i, done);
                    }
                    break;
                case 3:
                //west
                    if (neighborhood[4] == 1 && neighborhood[8] == 0)
                    {
                        removePoint(neighborhood, out + i, done);
                    }
                    break;
            }
        }

        mode = (mode + 1) % 4;
        memcpy(data, imData, sizeof(uint8_t) * width * height);
    } while (done == false);
    
    delete[] imData;

    // Change 0x01 -> 0xFF.
    regularize(data, width, height);
}

// Linear skeletonization algorithm
//
// This algorithm is inefficent, and was used only as a proof of concept
// during testing. In the main program, replace calls to Skeleton2() with
// this function if you want to see the program behave even more slowly.
//
// Parameters:
//              data   - 8 bit data, representing an image, where each byte is either 0x0 or 0xFF
//              width  - width of the image
//              height - height of the image
void Skeleton(uint8_t *data, uint16_t width, uint16_t height)
{
    uint8_t neighborhood[9];
    bool done = true;
    int area = width * height;
    int i;

    // Normalize data. 0xFF -> 1, all else = 0
    normalize(data, width, height);

    do
    {
        done = true;

        for (i=0; i < area; i++)
        {
            // Only consider cases where the center is 1
            if (data[i] == 1)
            {
                getNeighborhood(neighborhood, data, i, width, height);
            }
            else
            {
                continue;
            }

            if (chi(neighborhood) == 2 && sigma(neighborhood) != 1)
            {
                data[i] = 0;
                done = false;
            }
        }

    } while (done == false);

    // Change 0x01 -> 0xFF.
    regularize(data, width, height);
}
