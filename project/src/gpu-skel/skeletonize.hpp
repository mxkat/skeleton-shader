#ifndef __SKELETON_H__
#define __SKELETON_H__

#include <stdint.h>

// Linear skeletonization algorithm
//
// Adapted from E.R. Davies' book "Computer and Machine Vision", chapter 9.
// ISBN: 978-0123869081
//
// This algorithm is inefficent, and was used only as a proof of concept
// during testing. In the main program, replace calls to Skeleton2() with
// this function if you want to see the program behave even more slowly.
//
// Parameters:
//              data   - 8 bit data, representing an image, where each byte is either 0x0 or 0xFF
//              width  - width of the image
//              height - height of the image
void Skeleton(uint8_t *data, uint16_t width, uint16_t height);


// North-South-East-West skeletonization
//
// Adapted from E.R. Davies' book "Computer and Machine Vision", chapter 9.
// ISBN: 978-0123869081
//
// This is the improved version over Skeleton(). This function is parallelizable,
// and is ported to the compute shader for the GPU version.
//
// The N-S-E-W skeletonization removes all of the top pixels (north) it can,
// then removes all the right pixels (east) it can, etc, until no more pixels
// can be removed, leaving a skeleton image.
//
// Parameters:
//              data   - 8 bit data, representing an image, where each byte is either 0x0 or 0xFF
//              width  - width of the image
//              height - height of the image
void Skeleton2(uint8_t *data, uint16_t width, uint16_t height);

#endif // __SKELETON_H__
