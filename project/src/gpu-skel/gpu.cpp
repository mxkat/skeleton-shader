// Include standard headers
#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <math.h>
#include <errno.h>
#include <mutex>

//For stat(), open()
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>

#define GL_GLEXT_PROTOTYPES       //Necessary for OpenGL extensions

//Glut
#include <GL/glut.h>

//OpenCV
#include "opencv2/opencv.hpp"

//program specific
#include "skeletonize.hpp"

#ifdef DEBUG
#define checkError(x) _checkError(x)
#else
#define checkError(x)
#endif

using namespace std;
using namespace cv;               //OpenCV namespace
mutex cvLock;

// OpenCV camera info
static VideoCapture cap;          // Webcam interface
static unsigned int width = 640;  // Webcam resolution
static unsigned int height = 480; // Webcam resolution
static Mat colorFrame;            // Single webcam frame, in color
static Mat binaryFrame;           // Single webcam frame, "binary" (0 or 0xFF)
static Mat binary32bitFrame;      // Single webcam frame, "binary" (0 or 0xFF),
                                  //   converted to use 32 bit values, since the
                                  //   GPU natively uses in 32 bit values

static Ptr<BackgroundSubtractor> pMOG2;  // Background Subtractor algorithm


// Program modes
enum _program_modes { MODE_WEBCAM=0, MODE_BINARY, MODE_SKELETON, MODE_OVERLAY, MODE_END };
enum _skele_modes   { MODE_SOFTWARE, MODE_HARDWARE };

int mode         = MODE_WEBCAM;      // current viewing mode
int skeletonMode = MODE_SOFTWARE;   // cpu vs gpu computing switch

// OpenGL shader info
static GLuint shaderProgram;

// Texture info
#define TEX_SKELETON        0
#define TEX_WEBCAM          1
#define NUM_TEX             2

#define GL_TEXTURE_WEBCAM   GL_TEXTURE0
#define GL_UNIFORM_WEBCAM   0

#define GL_TEXTURE_SKELETON GL_TEXTURE1
#define GL_UNIFORM_SKELETON 1

GLuint textures[NUM_TEX];

// Compute shader info
#define COMPUTE_SHADER_SRC  "compute.shader"

#define IMG_0               0
#define IMG_1               1
#define CMD                 2
#define NUM_BUFS            3

#define CMD_BUF_WIDTH       0
#define CMD_BUF_HEIGHT      1
#define CMD_BUF_CMD         2
#define CMD_BUF_RESPONSE    3
#define CMD_BUF_LEN         4

#define CMD_TEST            1
#define CMD_THIN_N          2
#define CMD_THIN_S          3
#define CMD_THIN_E          4
#define CMD_THIN_W          5
#define CMD_NORMALIZE       6
#define CMD_REGULARIZE      7

#define INITIALIZED         0
#define NOT_FINISHED        1

static GLuint computeProgram;
GLuint buffers[NUM_BUFS];       //SSBO objects, one for IMG_0, one for IMG_1, and one for commands/response

// Shader sources
const GLchar* vertexSource =
    "#version 450 core\n"
    "in vec3 position;"
    "in vec2 texcoord;"
    "out vec2 Texcoord;"
    "void main()"
    "{"
    "    Texcoord = texcoord;"
    "    gl_Position = vec4(position, 1.0);"
    "}";

const GLchar* fragmentSource =
    "#version 450 core\n"
    "in vec2 Texcoord;"
    "out vec4 outColor;"
    "uniform sampler2D texData;"
    "void main()"
    "{"
    "   vec4 imColor = texture(texData, Texcoord);"
    "   if (imColor.a < 0.1) discard; "             // Drops alpha = 0 pixels
    "   outColor = vec4(imColor.r, imColor.g, imColor.b, imColor.a);"
    "}";

// Prints out any OpenGL errors. Used for debugging
void _checkError(int line)
{
    GLint err;

    do
    {
        err = glGetError();
        switch (err)
        {
            case GL_NO_ERROR:
                //printf("%d: No error\n", line);
                break;
            case GL_INVALID_ENUM:
                printf("%d: Invalid enum!\n", line);
                break;
            case GL_INVALID_VALUE:
                printf("%d: Invalid value\n", line);
                break;
            case GL_INVALID_OPERATION:
                printf("%d: Invalid operation\n", line);
                break;
            case GL_INVALID_FRAMEBUFFER_OPERATION:
                printf("%d: Invalid framebuffer operation\n", line);
                break;
            case GL_OUT_OF_MEMORY:
                printf("%d: Out of memory\n", line);
                break;
            default:
                printf("%d: glGetError default case. Should not happen!\n", line);
        }
    } while (err != GL_NO_ERROR);
}

// Loads a shader source file
//
// Parameters:
//              pFile - Filename to be loaded
//
// Returns: A pointer to the source or NULL on failure
GLchar* loadSource(const char* pFile)
{
    struct stat buf;
    GLchar *source;
    int fd;

    if (stat(pFile, &buf) == -1)
    {
        printf("Error opening file\n");
        printf("Error: %s\n", strerror(errno));
        return NULL;
    }

    fd = open(pFile, O_RDONLY);

    if (fd == -1)
    {
        printf("Error opening file. Error: %s\n", strerror(errno));
        return NULL;
    }

    source = new GLchar[buf.st_size + 1];

    if (read(fd, source, buf.st_size) == -1)
    {
        printf("Error reading file. Error: %s\n", strerror(errno));
        delete[] source;
        return NULL;
    }

    source[buf.st_size] = '\0'; //Shader compiler needs null to know end of input

    return source;
}

// Sets the texture parameters for GPU processing
void setupGPUTextureProcessing()
{
    glActiveTexture(GL_TEXTURE_SKELETON);
    glBindTexture(GL_TEXTURE_2D, textures[TEX_SKELETON]);

    glUniform1i(glGetUniformLocation(shaderProgram, "texData"), GL_UNIFORM_SKELETON);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    checkError(__LINE__);
}

// Sets the texture parameters for CPU processing
void setupCPUTextureProcessing()
{
    glActiveTexture(GL_TEXTURE_WEBCAM);
    glBindTexture(GL_TEXTURE_2D, textures[TEX_WEBCAM]);

    glUniform1i(glGetUniformLocation(shaderProgram, "texData"), GL_UNIFORM_WEBCAM);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    checkError(__LINE__);
}


// Initializes the compute program
//
// Returns: 0 on success, other values on failure
int initComputeProgram()
{
    GLchar *computeSource;

    printf("Attempting to load compute shader from file: %s\n", COMPUTE_SHADER_SRC);

    computeSource = loadSource(COMPUTE_SHADER_SRC);
    if (computeSource == NULL)
    {
        return 1;
    }
    GLuint computeShader = glCreateShader(GL_COMPUTE_SHADER);
    glShaderSource(computeShader, 1, &computeSource, NULL);
    glCompileShader(computeShader);


    computeProgram = glCreateProgram();
    glAttachShader(computeProgram, computeShader);
    glLinkProgram(computeProgram);
    GLint status;
    glGetProgramiv(computeProgram, GL_LINK_STATUS, &status);

    if (status == GL_TRUE)
    {
        printf("Compute shader loaded\n");
    }
    else
    {
        printf("Compute shader linking failed\n");
        GLchar log[4096];
        GLsizei len;

        glGetProgramInfoLog(computeProgram, 4096, &len, log);

        printf("%s\n", log);
        return 1;
    }

    // Initialize the SSBO buffers
    glGenBuffers(NUM_BUFS, buffers);

    return 0;
}

// Runs the GPU version of the skeletonization algorithm.
//
// Parameters:
//              data - 32 bit unsigned array representing the entire image
void runComputeProgram(void *data)
{
    uint32_t *ptr;
    uint32_t cmd[CMD_BUF_LEN];

    // First action is to transform the image into binary values (0, 1)

    cmd[CMD_BUF_CMD] = CMD_NORMALIZE;
    cmd[CMD_BUF_WIDTH] = width;
    cmd[CMD_BUF_HEIGHT] = height;

    glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 0, buffers[CMD]);
    glBufferData(GL_SHADER_STORAGE_BUFFER, sizeof(cmd), cmd, GL_DYNAMIC_DRAW);

    glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 1, buffers[IMG_0]);
    glBufferData(GL_SHADER_STORAGE_BUFFER, sizeof(uint32_t) * width * height, data, GL_DYNAMIC_DRAW);

    glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 2, buffers[IMG_1]);
    glBufferData(GL_SHADER_STORAGE_BUFFER, sizeof(uint32_t) * width * height, data, GL_DYNAMIC_DRAW);


    glUseProgram(computeProgram);

    glDispatchCompute(width / 16, height / 16, 1);
    glMemoryBarrier(GL_SHADER_STORAGE_BARRIER_BIT);


    // Main driver of the compute shader. We are "ping-ponging" the data
    // between two buffers, keeping as much execution and data as possible
    // on the GPU. Once the skeletonization algorithm reports back there
    // has been no modification to the image, the loop terminates.

    // Bind ptr for our while loop
    glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 0, buffers[CMD]);
    ptr = (uint32_t *)glMapBuffer(GL_SHADER_STORAGE_BUFFER, GL_READ_ONLY);

    int i = 0;
    do
    {
        glUnmapBuffer(GL_SHADER_STORAGE_BUFFER);

        // Create command for the compute shader
        cmd[CMD_BUF_RESPONSE] = INITIALIZED;

        switch (i % 4)
        {
            case 0:
                cmd[CMD_BUF_CMD] = CMD_THIN_N;
                break;
            case 1:
                cmd[CMD_BUF_CMD] = CMD_THIN_S;
                break;
            case 2:
                cmd[CMD_BUF_CMD] = CMD_THIN_E;
                break;
            case 3:
                cmd[CMD_BUF_CMD] = CMD_THIN_W;
                break;
        }

        // Send the command down to the GPU
        glBufferData(GL_SHADER_STORAGE_BUFFER, sizeof(cmd), cmd, GL_DYNAMIC_DRAW);

        // Switch input/output buffers based on loop iteration
        if (i % 2 == 0)
        {
            glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 1, buffers[IMG_1]);
            glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 2, buffers[IMG_0]);
        }
        else
        {
            glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 1, buffers[IMG_0]);
            glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 2, buffers[IMG_1]);
        }

        // Run the shader
        glDispatchCompute(width / 16, height / 16, 1);
        glMemoryBarrier(GL_SHADER_STORAGE_BARRIER_BIT);

        // Determine whether to stop
        glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 0, buffers[CMD]);
        ptr = (uint32_t *)glMapBuffer(GL_SHADER_STORAGE_BUFFER, GL_READ_ONLY);
        i++;
    } while(ptr[CMD_BUF_RESPONSE] != INITIALIZED);

    glUnmapBuffer(GL_SHADER_STORAGE_BUFFER); // Free ptr

    // Transform Binary image (0, 1) to (0, 0xFFFFFFFF - RGBA) values for texture display
    cmd[CMD_BUF_CMD] = CMD_REGULARIZE;
    glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 0, buffers[CMD]);
    glBufferData(GL_SHADER_STORAGE_BUFFER, sizeof(cmd), cmd, GL_DYNAMIC_DRAW);

    glDispatchCompute(width / 16, height / 16, 1);
    glMemoryBarrier(GL_SHADER_STORAGE_BARRIER_BIT);

    // Create texure

    glActiveTexture(GL_TEXTURE_SKELETON);
    glBindTexture(GL_TEXTURE_2D, textures[TEX_SKELETON]);

    if (i % 2 == 0)
    {
        // output image is IMG_0
        glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 2, buffers[IMG_0]);
        ptr = (uint32_t *)glMapBuffer(GL_SHADER_STORAGE_BUFFER, GL_READ_ONLY);

    }
    else
    {
        // output image is IMG_1
        glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 2, buffers[IMG_1]);
        ptr = (uint32_t *)glMapBuffer(GL_SHADER_STORAGE_BUFFER, GL_READ_ONLY);
    }

    glUnmapBuffer(GL_SHADER_STORAGE_BUFFER);

    glUseProgram(shaderProgram);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0,  GL_RGBA,  GL_UNSIGNED_BYTE, ptr);
    checkError(__LINE__);

}



// Displays the result of the current mode and GPU/CPU combination
void display()
{
    cvLock.lock();

    Mat *pFrame;

	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
	glClear(GL_COLOR_BUFFER_BIT);


    switch (mode)
    {
        case MODE_BINARY:
            // Shows the binary image from processing the webcam

            glActiveTexture(GL_TEXTURE_WEBCAM);
            pFrame = &binaryFrame;
            if (pFrame->data == NULL)
            {
               break;
            }
            glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB8, width, height, 0,
                         GL_LUMINANCE,  GL_UNSIGNED_BYTE,
                         (unsigned char *)pFrame->data);

            glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0);
            break;
        case MODE_WEBCAM:
            // Shows the webcam raw input

            glActiveTexture(GL_TEXTURE_WEBCAM);
            pFrame = &colorFrame;
            glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB8, width, height, 0,  GL_BGR,
                         GL_UNSIGNED_BYTE, (unsigned char *)pFrame->data);

            glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0);

            glColor3f(1.0, 1.0, 1.0);
            break;
        case MODE_SKELETON:
            // Shows either the GPU or CPU skeleton

            pFrame = &binaryFrame;
            if (pFrame->data == NULL)
            {
               break;
            }
            if (skeletonMode == MODE_SOFTWARE)
            {
                // CPU computation
                glActiveTexture(GL_TEXTURE_WEBCAM);
                Skeleton2(pFrame->data, width, height);

                glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB8, width, height, 0,
                             GL_LUMINANCE,  GL_UNSIGNED_BYTE,
                             (unsigned char *)pFrame->data);

                glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0);
            }
            else
            {
                // GPU computation
                pFrame = &binary32bitFrame;
                glActiveTexture(GL_TEXTURE_SKELETON);
                runComputeProgram(pFrame->data);

                glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0);
            }
            break;
        case MODE_OVERLAY:
            //Overlays the GPU skeleton on top of the webcam image

            glActiveTexture(GL_TEXTURE_WEBCAM);
            setupCPUTextureProcessing();
            pFrame = &colorFrame;
            glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB8, width, height, 0,  GL_BGR,
                         GL_UNSIGNED_BYTE, (unsigned char *)pFrame->data);

            glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0);

            glActiveTexture(GL_TEXTURE_SKELETON);
            setupGPUTextureProcessing();
            pFrame = &binary32bitFrame;
            runComputeProgram(pFrame->data);

            glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0);
            break;
    }


    glFlush();
    glutSwapBuffers();
    cvLock.unlock();
}


// Gets a frame from the webcam and generates required data for the
// skeletonization algorithms.
//
// The flow is to grab the frame, do a Gaussian blur to reduce noise,
// create a difference mask (remove background), transform the image into
// a grayscale frame, and finally threshold and turn the grayscale into
// a binary frame.
//
// We also create a 32 bit version of the binary frame. This is because the
// CPU version of the skeleton algorithm handles 8 bit data, the GPU version
// must have 32 bit data, sine GLSL's smallest unit is 32 bits.
//
// Converting the 8 bit data to 32 bit could be done in the GPU, but this
// was deemed a waste of time. This function performs well enough.
void getCameraFrame()
{
    Mat fgMaskMOG2;     // Mask for background removal
    Mat thresh;         // Used for applying a threshold to grayscale image
    Mat maskedFrame;    // Used for masking off the background

    cvLock.lock();

    cap >> colorFrame;

    // Get a color frame and transform it also into a binary frame for
    // use with the skeletonization algorithm.
    GaussianBlur(colorFrame, thresh, Size(5, 5), 3, 3);        // Reduces noise
    pMOG2->operator()(thresh, fgMaskMOG2, 0);                  // Create mask
    colorFrame.copyTo(maskedFrame, fgMaskMOG2);                // Mask frame
    cvtColor(maskedFrame, thresh, CV_BGR2GRAY);                // Turn Gray
    threshold(thresh, binaryFrame, 0x40, 0xFF, THRESH_BINARY); // Create binary

    // Create the 32 bit frame for the GPU conversion. We are using a 32 bit
    // signed integer since OpenCV does not support a 32 bit unsigned int.
    // However, it shouldn't matter, since the GPU skeletonization will
    // convert everything to it's proper value (0 or 1)
    binaryFrame.convertTo(binary32bitFrame, CV_32S);


    cvLock.unlock();
    glutPostRedisplay();

}


// Handles re-sizing the window
void reshape(int width,int height)
{
    //  Set viewport as entire window
    glViewport(0,0, width,height);
    glutPostRedisplay();
}

// Converts the mode enums into a string for printing
void modeToStr(int mode, char *buf)
{
    switch (mode)
    {
        case MODE_WEBCAM:
            strcpy(buf, "Webcam");
            break;
        case MODE_BINARY:
            strcpy(buf, "Binary Image");
            break;
        case MODE_SKELETON:
            strcpy(buf, "Skeleton");
            break;
        case MODE_OVERLAY:
            strcpy(buf, "Overlay");
            break;
        default:
            strcpy(buf, "");
            break;
    }
}

// Converts the skeleton mode enums into a string for printing
void skeletonModeToStr(int mode, char *buf)
{
    switch (mode)
    {
        case MODE_SOFTWARE:
            strcpy(buf, "Software");
            break;
        case MODE_HARDWARE:
            strcpy(buf, "Hardware");
            break;
        default:
            strcpy(buf, "");
            break;
    }
}

// Handles keypresses from GLUT
void key(unsigned char ch,int x,int y)
{
    char modeStr[20];
    char skeletonStr[20];

    switch (ch)
    {
        case 'q':
            exit(0);
            break;
        case 'm':
            // Switch modes
            mode = (mode + 1) % MODE_END;
            if (mode == MODE_WEBCAM)
            {
                // When we wrap around to the beginning, we need to reset
                // the CPU/GPU processing, otherwise we'll get a black square.
                skeletonMode = MODE_SOFTWARE;
                setupCPUTextureProcessing();
            }
            break;
        case 's':
            // Switch between hardware and software processing
            if (skeletonMode == MODE_SOFTWARE)
            {
                // Switch to GPU processing
                setupGPUTextureProcessing();
                skeletonMode = MODE_HARDWARE;
            }
            else
            {
                // Switch to CPU processing
                setupCPUTextureProcessing();
                skeletonMode = MODE_SOFTWARE;
            }
            break;
    }

    modeToStr(mode, modeStr);
    skeletonModeToStr(skeletonMode, skeletonStr);

    printf("Mode: %s, Processor: %s\n", modeStr, skeletonStr);
    glutPostRedisplay();
}


int main(int argc, char** argv)
{
    // Initialize the background subtractor from OpenCV
    printf("Initializing program\n");
    pMOG2 = new BackgroundSubtractorMOG();

    if (argc == 1)
    {
        cap.open(0);  // open the default camera
        width = 640;  // default width
        height = 480; // default height
    }
    else if (argc == 2)
    {

        cap.open(atoi(argv[1]));
    }
    else if (argc == 4)
    {
        cap.open(atoi(argv[1]));
        width = atoi(argv[2]);
        height = atoi(argv[3]);
    }
    else
    {
        printf("Syntax error: use %s [camera number] [width] [height]\n", argv[0]);
        return 1;
    }

    if(!cap.isOpened())  // check if we succeeded
    {
        printf("Error: Could not open camera number %s\n", argv[1]);
        return 1;
    }

    printf("Initializing camera\n");
    if (cap.set(CV_CAP_PROP_FRAME_WIDTH,width) == true)
    {
        printf("Error: Could not set camera width\n");
        return 1;
    }

    if (cap.set(CV_CAP_PROP_FRAME_HEIGHT,height) == true)
    {
        printf("Error: Could not set camera height\n");
        return 1;
    }

	cap >> colorFrame;

    // Window Setup

    printf("Initializing OpenGL/GLUT\n");
    glutInitWindowSize(width, height);
    glutInitWindowPosition (140, 140);
    glutInitDisplayMode(GLUT_RGB | GLUT_DOUBLE);
    glutInit(&argc, argv);
    glutCreateWindow( "Sean Hicks - Realtime Skeletonization Shader" );

    // Glut Callbacks
    glutDisplayFunc(display);
    glutReshapeFunc(reshape);
    glutKeyboardFunc(key);
    glutIdleFunc(getCameraFrame);

    // Initialize the compute shader program
    initComputeProgram();

    // Vertices & texture init
    GLuint vao;
    glGenVertexArrays(1, &vao);
    glBindVertexArray(vao);

    GLuint vbo;
    glGenBuffers(1, &vbo);

    GLfloat vertices[] = {
        // X    Y      Z     S     T
        -1.0f,  1.0f, 0.0f, 0.0f, 0.0f, // Top-left
         1.0f,  1.0f, 0.0f, 1.0f, 0.0f, // Top-right
         1.0f, -1.0f, 0.0f, 1.0f, 1.0f, // Bottom-right
        -1.0f, -1.0f, 0.0f, 0.0f, 1.0f  // Bottom-left
    };


    glBindBuffer(GL_ARRAY_BUFFER, vbo);
    glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);

    GLuint ebo;
    glGenBuffers(1, &ebo);

    GLuint elements[] = {
        0, 1, 2,
        2, 3, 0
    };

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ebo);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(elements), elements, GL_STATIC_DRAW);

    // Create shaders

    GLuint vertexShader = glCreateShader(GL_VERTEX_SHADER);
    glShaderSource(vertexShader, 1, &vertexSource, NULL);
    glCompileShader(vertexShader);

    GLuint fragmentShader = glCreateShader(GL_FRAGMENT_SHADER);
    glShaderSource(fragmentShader, 1, &fragmentSource, NULL);
    glCompileShader(fragmentShader);

    shaderProgram = glCreateProgram();
    glAttachShader(shaderProgram, vertexShader);
    glAttachShader(shaderProgram, fragmentShader);
    glBindFragDataLocation(shaderProgram, 0, "outColor");
    glLinkProgram(shaderProgram);
    glUseProgram(shaderProgram);

    // Vertex data specification
    GLint posAttrib = glGetAttribLocation(shaderProgram, "position");
    glEnableVertexAttribArray(posAttrib);
    glVertexAttribPointer(posAttrib, 2, GL_FLOAT, GL_FALSE, 5 * sizeof(GLfloat), 0);

    GLint texAttrib = glGetAttribLocation(shaderProgram, "texcoord");
    glEnableVertexAttribArray(texAttrib);
    glVertexAttribPointer(texAttrib, 2, GL_FLOAT, GL_FALSE, 5 * sizeof(GLfloat), (void *)(3 * sizeof(GLfloat)));

    checkError(__LINE__);

    // Load Textures
    glGenTextures(NUM_TEX, textures);
    glActiveTexture(GL_TEXTURE_WEBCAM);
    glBindTexture(GL_TEXTURE_2D, textures[TEX_WEBCAM]);

    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB8, width, height, 0,
                 GL_LUMINANCE,  GL_UNSIGNED_BYTE,
                 (unsigned char *)colorFrame.data);

    glUniform1i(glGetUniformLocation(shaderProgram, "texData"), GL_UNIFORM_WEBCAM);

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

    // Start program
    printf("Initialization complete. Running program\n");
    checkError(__LINE__);
    glutMainLoop();

    // Teardown

    glDeleteTextures(2, textures);

    glDeleteProgram(shaderProgram);
    glDeleteShader(fragmentShader);
    glDeleteShader(vertexShader);

    glDeleteBuffers(1, &ebo);
    glDeleteBuffers(1, &vbo);

    glDeleteVertexArrays(1, &vao);

    glDeleteBuffers(NUM_BUFS, buffers);
    glDeleteProgram(computeProgram);

    return 0;
}

