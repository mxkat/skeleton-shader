#include <stdint.h>
#include <inttypes.h>
#include <string.h> //memcpy
#include <stdio.h> //printf -- TODO: remove

int sigma(uint8_t *data)
{
    int i;
    int sigma = 0;

    // Assume 9 pixels, A0 (pixel of interest) -> A8
    // In image, A0 is center
    // 1 2 3
    // 8 0 4
    // 7 6 5

    for (i=1; i < 9; i++)
    {
        sigma += data[i];
    }

    return sigma;
}


int chi(uint8_t *data)
{
    int chi;

    // Assume 9 pixels, A0 (pixel of interest) -> A8
    // 1 2 3
    // 8 0 4
    // 7 6 5

    chi = (int) (data[1] != data[3]) +
          (int) (data[3] != data[5]) +
          (int) (data[5] != data[7]) +
          (int) (data[7] != data[1]) +

          2 * ( (int) ((data[2] > data[1]) && (data[2] > data[3])) ) +
          (int) ((data[4] > data[3]) && (data[4] > data[5])) +
          (int) ((data[6] > data[5]) && (data[6] > data[7])) +
          (int) ((data[8] > data[7]) && (data[8] > data[1]));

   return chi;
}


// 1 2 3
// 8 0 4
// 7 6 5

// neighborhood - the 9 element array representing the 3x3 neighborhood
// data         - the whole image
// pos          - current (1D) position in the data array
// width        - width of the image
// height       - height of the image
void getNeighborhood(uint8_t *neighborhood, uint8_t *data, int pos, uint16_t width, uint16_t height)
{
    int bottom = width * height - width; // Offset of bottom row
    int i;

    neighborhood[0] = data[pos];

    for (i=1; i < 9; i++)
    {
        // Mark all of the neighborhood as '2'. We will get any pixels
        // that are still marked this after we check the edges
        neighborhood[i] = 2;
    }

    if (pos < width)
    {
        // Pixel is on top row. Fill area outside of image with zero
        neighborhood[1] = 0;
        neighborhood[2] = 0;
        neighborhood[3] = 0;
    }

    if (pos % width == 0)
    {
        // Pixel is on left edge. Fill area outside of image with zero
        neighborhood[1] = 0;
        neighborhood[8] = 0;
        neighborhood[7] = 0;
    }

    if ((pos % width) == (width - 1))
    {
        // Pixel is on right edge.
        neighborhood[3] = 0;
        neighborhood[4] = 0;
        neighborhood[5] = 0;
    }

    if (pos >= bottom)
    {
        // Pixel is on bottom edge.
        neighborhood[5] = 0;
        neighborhood[6] = 0;
        neighborhood[7] = 0;
    }

    // Get remaining pixels
    for (i=1; i < 9; i++)
    {
        if (neighborhood[i] == 2)
        {
            switch (i)
            {
                case 1:
                    // Upper left pixel
                    neighborhood[i] = data[pos - 1 - width];
                    break;
                case 2:
                    // Upper middle pixel
                    neighborhood[i] = data[pos - width];
                    break;
                case 3:
                    // Upper right pixel
                    neighborhood[i] = data[pos + 1 - width];
                    break;
                case 4:
                    // Right pixel
                    neighborhood[i] = data[pos + 1];
                    break;
                case 5:
                    // Bottom right pixel
                    neighborhood[i] = data[pos + width + 1];
                    break;
                case 6:
                    // Bottom middle pixel
                    neighborhood[i] = data[pos + width];
                    break;
                case 7:
                    // Bottom left pixel
                    neighborhood[i] = data[pos + width - 1];
                    break;
                case 8:
                    // Left pixel
                    neighborhood[i] = data[pos - 1];
                    break;
            }
        }
    }
}

void regularize(uint8_t *data, uint16_t width, uint16_t height)
{
    int i;
    int area = width * height;

    for (i=0; i < area; i++)
    {
        if (data[i] == 1)
        {
            data[i] = 0xFF;
        }
    }
}

void normalize(uint8_t *data, uint16_t width, uint16_t height)
{
    int i;
    int area = width * height;

    for (i=0; i < area; i++)
    {
        if (data[i] == 0xFF)
        {
            data[i] = 1;
        }
        else
        {
            data[i] = 0;
        }
    }
}

void Threshold(uint8_t *data, uint16_t width, uint16_t height)
{
    uint8_t threshold = 0x1;
    int area = width * height;
    int i;

    for (i=0; i < area; i++)
    {
        if (data[i] >= threshold)
        {
            data[i] = 0xFF;
        }
        else
        {
            data[i] = 0x00;
        }
    }
}

void GaussianBlur(uint8_t *data, uint16_t width, uint16_t height)
{
    uint8_t neighborhood[9];
    uint8_t output = 0;
    uint8_t gaussMatrix[9] = { 1, 1, 1, 1, 1, 1, 1, 1, 1 };
    uint8_t weight = 9;
    int i, j, area;

    // Apply the Gaussian matrix. This is a convolution of
    //     1   [ 1 2 1 ]
    //     _   [ 2 4 2 ]
    //     16  [ 1 2 1 ]

    area = width * height;

    for (i=0; i < area; i++)
    {
        getNeighborhood(neighborhood, data, i, width, height);
        for (j=0; j < 9; j++)
        {
            output += neighborhood[j] * gaussMatrix[j];
        }

        output /= weight;

        data[i] = output;
    }
}

int removePoint(uint8_t* neighborhood, uint8_t *data, bool &done)
{
    if (chi(neighborhood) == 2 && sigma(neighborhood) != 1)
    {
        *data = 0;
        done = false;
        return 1;
    }
    return 0;
}

// North-South-East-West skeletonization - NW & SE grouped together
// This produces disjoint areas. Skeleton2 is the only one that seems to
// be guaranteed to make a whole skeleton
void Skeleton4(uint8_t *data, uint16_t width, uint16_t height)
{
    uint8_t neighborhood[9];
    bool done = true;
    int area = width * height;
    int i;
    int mode = 0; // 0 = north, 2 = south, 3 = east, 4 = west

    // We will create two extra copies of the memory. When
    uint8_t *imData;
    uint8_t *imData2;
    uint8_t *swap;
    uint8_t *in;
    uint8_t *out;

    // Normalize data. 0xFF -> 1, all else = 0
    normalize(data, width, height);

    imData = new uint8_t[width * height];



    in = data;
    out = imData;

    //GaussianBlur(data, width, height);
    //Threshold(data, width, height);

    int j = 0;
    do
    {
        memcpy(imData, data, sizeof(uint8_t) * width * height);
        done = true;

        for (i=0; i < area; i++)
        {
            // Only consider cases where the center is 1
            if (in[i] == 1)
            {
                getNeighborhood(neighborhood, in, i, width, height);
            }
            else
            {
                continue;
            }

            switch (mode)
            {
                case 0:
                case 2:
                //north
                    if (neighborhood[2] == 0 && neighborhood[6] == 1)
                    {

                        removePoint(neighborhood, out + i, done);

                    }
                //west
                    if (neighborhood[4] == 1 && neighborhood[8] == 0)
                    {
                        removePoint(neighborhood, out + i, done);
                    }
                    break;

                case 1:
                case 3:
                //south
                    if (neighborhood[2] == 1 && neighborhood[6] == 0)
                    {
                        removePoint(neighborhood, out + i, done);
                    }


                //east
                    if (neighborhood[4] == 0 && neighborhood[8] == 1)
                    {
                        removePoint(neighborhood, out + i, done);
                    }
                    break;

            }
        }

        mode = (mode + 1) % 4;
        //printf("Switching to mode: %d\n", mode); //TODO: remove

      /*
        swap = in;
        in = out;
        out = swap;
        j++;
      */
        memcpy(data, imData, sizeof(uint8_t) * width * height);
    } while (done == false);
    //} while (0);

/*
    if (in != data)
    {
        memcpy(data, in, sizeof(uint8_t) * width * height);
    }
*/
    delete[] imData;

    // Change 0x01 -> 0xFF.
    regularize(data, width, height);
}


// Same as Skeleton2, but with all NSWE testing done at the same time.
// Skeleton2 is better. This one produces a disjoint skeleton.
void Skeleton3(uint8_t *data, uint16_t width, uint16_t height)
{
    uint8_t neighborhood[9];
    bool done = true;
    int area = width * height;
    int i;
    int mode = 0; // 0 = north, 2 = south, 3 = east, 4 = west

    // We will create two extra copies of the memory. When
    uint8_t *imData;
    uint8_t *imData2;
    uint8_t *swap;
    uint8_t *in;
    uint8_t *out;

    // Normalize data. 0xFF -> 1, all else = 0
    normalize(data, width, height);

    imData = new uint8_t[width * height];



    in = data;
    out = imData;

    //GaussianBlur(data, width, height);
    //Threshold(data, width, height);

    int j = 0;
    do
    {
        memcpy(imData, data, sizeof(uint8_t) * width * height);
        done = true;

        for (i=0; i < area; i++)
        {
            // Only consider cases where the center is 1
            if (in[i] == 1)
            {
                getNeighborhood(neighborhood, in, i, width, height);
            }
            else
            {
                continue;
            }

            //north
            if (neighborhood[2] == 0 && neighborhood[6] == 1)
            {
                removePoint(neighborhood, out + i, done);
            }


            //south
            if (neighborhood[2] == 1 && neighborhood[6] == 0)
            {
                removePoint(neighborhood, out + i, done);
            }

            //east
            if (neighborhood[4] == 0 && neighborhood[8] == 1)
            {
                removePoint(neighborhood, out + i, done);
            }

            //west
            if (neighborhood[4] == 1 && neighborhood[8] == 0)
            {
                removePoint(neighborhood, out + i, done);
            }
        }


        memcpy(data, imData, sizeof(uint8_t) * width * height);
    } while (done == false);

    delete[] imData;

    // Change 0x01 -> 0xFF.
    regularize(data, width, height);
}


// North-South-East-West skeletonization
void Skeleton2(uint8_t *data, uint16_t width, uint16_t height)
{
    uint8_t neighborhood[9];
    bool done = true;
    int area = width * height;
    int i;
    int mode = 0; // 0 = north, 2 = south, 3 = east, 4 = west

    // We will create two extra copies of the memory. When
    uint8_t *imData;
    uint8_t *imData2;
    uint8_t *swap;
    uint8_t *in;
    uint8_t *out;

    // Normalize data. 0xFF -> 1, all else = 0
    normalize(data, width, height);

    imData = new uint8_t[width * height];



    in = data;
    out = imData;

    //GaussianBlur(data, width, height);
    //Threshold(data, width, height);

    int j = 0;
    do
    {
        memcpy(imData, data, sizeof(uint8_t) * width * height);
        done = true;

        for (i=0; i < area; i++)
        {
            // Only consider cases where the center is 1
            if (in[i] == 1)
            {
                getNeighborhood(neighborhood, in, i, width, height);
            }
            else
            {
                continue;
            }

            switch (mode)
            {
                case 0:
                //north
                    if (neighborhood[2] == 0 && neighborhood[6] == 1)
                    {

                        removePoint(neighborhood, out + i, done);

                    }
                    break;
                case 1:
                //south
                    if (neighborhood[2] == 1 && neighborhood[6] == 0)
                    {
                        removePoint(neighborhood, out + i, done);
                    }
                    break;
                case 2:
                //east
                    if (neighborhood[4] == 0 && neighborhood[8] == 1)
                    {
                        removePoint(neighborhood, out + i, done);
                    }
                    break;
                case 3:
                //west
                    if (neighborhood[4] == 1 && neighborhood[8] == 0)
                    {
                        removePoint(neighborhood, out + i, done);
                    }
                    break;
            }
        }

        mode = (mode + 1) % 4;
        //printf("Switching to mode: %d\n", mode); //TODO: remove

      /*
        swap = in;
        in = out;
        out = swap;
        j++;
      */
        memcpy(data, imData, sizeof(uint8_t) * width * height);
    } while (done == false);
    //} while (0);

/*
    if (in != data)
    {
        memcpy(data, in, sizeof(uint8_t) * width * height);
    }
*/
    delete[] imData;

    // Change 0x01 -> 0xFF.
    regularize(data, width, height);
}


void Skeleton(uint8_t *data, uint16_t width, uint16_t height)
{
    uint8_t neighborhood[9];
    bool done = true;
    int area = width * height;
    int i;

    //GaussianBlur(data, width, height);
    //Threshold(data, width, height);

    // Normalize data. 0xFF -> 1, all else = 0
    normalize(data, width, height);

    do
    {
        done = true;

        for (i=0; i < area; i++)
        {
            // Only consider cases where the center is 1
            if (data[i] == 1)
            {
                getNeighborhood(neighborhood, data, i, width, height);
            }
            else
            {
                continue;
            }

            if (chi(neighborhood) == 2 && sigma(neighborhood) != 1)
            {
                data[i] = 0;
                done = false;
            }
        }

    } while (done == false);

    // Change 0x01 -> 0xFF.
    regularize(data, width, height);
}


/*
// Test setup
#include "PGM.hpp"

int main()
{
    int i, area;
    int pre_count=0;
    int post_count=0;

    uint8_t *data;

    PGM pgmImage;
    pgmImage.ReadFile("test.pgm");

    data = pgmImage.GetData();
    area = pgmImage.GetWidth() * pgmImage.GetHeight();

    for (i=0; i < area; i++)
    {
        if (data[i] == 0xFF)
        {
            pre_count++;
        }
    }


    Skeleton2(pgmImage.GetData(), pgmImage.GetWidth(), pgmImage.GetHeight());

    data = pgmImage.GetData();
    area = pgmImage.GetWidth() * pgmImage.GetHeight();

    for (i=0; i < area; i++)
    {
        if (data[i] == 0xFF)
        {
            post_count++;
        }
    }

    printf("pre: %d post: %d\n", pre_count, post_count);

    return 0;
}
*/
