// Include standard headers
#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <math.h>
#include <errno.h>

//For stat
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>

// Include GLEW
#include <GL/glew.h>

//Glut
#include <GL/glut.h>

//Project specific
#include "skeletonize.hpp"
#include "PGM.hpp"

// OpenGL shader info
GLuint programID;
GLuint output_image;

#define IMG_0               0
#define IMG_1               1
#define CMD                 2
#define NUM_BUFS            3

#define CMD_BUF_WIDTH       0
#define CMD_BUF_HEIGHT      1
#define CMD_BUF_CMD         2
#define CMD_BUF_RESPONSE    3
#define CMD_BUF_LEN         4

#define CMD_EXPAND          1
#define CMD_THIN_N          2
#define CMD_THIN_S          3
#define CMD_THIN_E          4
#define CMD_THIN_W          5
#define CMD_NORMALIZE       6
#define CMD_REGULARIZE      7

#define INITIALIZED         0
#define NOT_FINISHED        1

GLuint computeProgram;
GLuint buffers[NUM_BUFS];       //SSBO objects, one for IMG_0, one for IMG_1, and one for commands/response
static GLchar* computeSource;
GLuint shaderProgram;


//TODO: don't really need 2 textures yet, but will eventually when doing overlay of original image.
GLuint textures[2];



GLchar* LoadSource(const char* pFile)
{
    struct stat buf;
    GLchar *source;
    int fd;

    if (stat(pFile, &buf) == -1)
    {
        printf("Error opening file\n");
        printf("Error: %s\n", strerror(errno));
        return NULL;
    }

    fd = open(pFile, O_RDONLY);

    if (fd == -1)
    {
        printf("Error opening file. Error: %s\n", strerror(errno));
        return NULL;
    }

    source = new GLchar[buf.st_size + 1];

    if (read(fd, source, buf.st_size) == -1)
    {
        printf("Error reading file. Error: %s\n", strerror(errno));
        delete[] source;
        return NULL;
    }

    source[buf.st_size] = '\0'; //Shader compiler needs null to know end of input

    return source;
}


// Shader sources
const GLchar* vertexSource =
    "#version 450 core\n"
    "in vec2 position;"
    "in vec2 texcoord;"
    "out vec2 Texcoord;"
    "void main()"
    "{"
    "    Texcoord = texcoord;"
    "    gl_Position = vec4(position, 0.0, 1.0);"
    "}";

const GLchar* fragmentSource =
    "#version 450 core\n"
    "in vec2 Texcoord;"
    "out vec4 outColor;"
    "uniform sampler2D texData;"
    "void main()"
    "{"
    "   vec4 imColor = texture(texData, Texcoord);"
    "   outColor = vec4(0.0, imColor.r, 0.0, 1.0);"
    //"    outColor = texture(texData, Texcoord);"
    //"    outColor = vec4(1.0, 1.0, 0.0, 1.0);"
    "}";


void checkError(int line)
{
    GLint err;

    do
    {
        err = glGetError();
        switch (err)
        {
            case GL_NO_ERROR:
                //printf("%d: No error\n", line);
                break;
            case GL_INVALID_ENUM:
                printf("%d: Invalid enum!\n", line);
                break;
            case GL_INVALID_VALUE:
                printf("%d: Invalid value\n", line);
                break;
            case GL_INVALID_OPERATION:
                printf("%d: Invalid operation\n", line);
                break;
            case GL_INVALID_FRAMEBUFFER_OPERATION:
                printf("%d: Invalid framebuffer operation\n", line);
                break;
            case GL_OUT_OF_MEMORY:
                printf("%d: Out of memory\n", line);
                break;
            default:
                printf("%d: glGetError default case. Should not happen!\n", line);
        }
    } while (err != GL_NO_ERROR);
}

void display()
{

	glClearColor(0.0f, 0.0f, 1.0f, 0.0f);

	glClear(GL_COLOR_BUFFER_BIT);

    glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0);

    glFlush();
    glutSwapBuffers();

}


void reshape(int width,int height)
{
    double w2h = (height>0) ? (double)width/height : 1;
    //  Set viewport as entire window
    glViewport(0,0, width,height);

}




void runComputeProgram(uint32_t *data, uint32_t *data2)
{
    int width = 640;
    int height = 400;

    uint32_t *ptr;
    uint32_t cmd[CMD_BUF_LEN];

    computeSource = LoadSource("compute.shader");
    if (computeSource == NULL)
    {
        return;
    }
    GLuint computeShader = glCreateShader(GL_COMPUTE_SHADER);
    glShaderSource(computeShader, 1, &computeSource, NULL);
    glCompileShader(computeShader);


    computeProgram = glCreateProgram();
    glAttachShader(computeProgram, computeShader);
    glLinkProgram(computeProgram);
    GLint status;
    glGetProgramiv(computeProgram, GL_LINK_STATUS, &status);

    if (status == GL_TRUE)
    {
        printf("link good\n");
    }
    else
    {
        printf("link bad\n");
        GLchar log[4096];
        GLsizei len;

        glGetProgramInfoLog(computeProgram, 4096, &len, log);

        printf("%s\n", log);
        return;
    }

    // First action is to transform the image into binary values (0, 1)

    cmd[CMD_BUF_CMD] = CMD_NORMALIZE;
    cmd[CMD_BUF_WIDTH] = width;
    cmd[CMD_BUF_HEIGHT] = height;

    glGenBuffers(NUM_BUFS, buffers);

    glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 0, buffers[CMD]);
    glBufferData(GL_SHADER_STORAGE_BUFFER, sizeof(cmd), cmd, GL_DYNAMIC_DRAW);

    glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 1, buffers[IMG_0]);
    glBufferData(GL_SHADER_STORAGE_BUFFER, sizeof(uint32_t) * width * height, data, GL_DYNAMIC_DRAW);

    glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 2, buffers[IMG_1]);
    glBufferData(GL_SHADER_STORAGE_BUFFER, sizeof(uint32_t) * width * height, data2, GL_DYNAMIC_DRAW);


    glUseProgram(computeProgram);

    glDispatchCompute(width / 16, height / 16, 1);
    glMemoryBarrier(GL_SHADER_STORAGE_BARRIER_BIT);



    glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 2, buffers[IMG_1]);
    ptr = (uint32_t *)glMapBuffer(GL_SHADER_STORAGE_BUFFER, GL_READ_ONLY);

    // Just checking to see that we match all of the pixels in the source image.
    // TODO: remove
    int count = 0;
    for (int i =0; i < width * height; i ++)
    {
        if (ptr[i] == 0x1)
        {
            count++;
        }
    }

    printf("count: %d\n", count);
    glUnmapBuffer(GL_SHADER_STORAGE_BUFFER);


    // Rebind ptr for our while loop
    glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 0, buffers[CMD]);
    //glBufferData(GL_SHADER_STORAGE_BUFFER, sizeof(cmd), cmd, GL_DYNAMIC_DRAW);
    ptr = (uint32_t *)glMapBuffer(GL_SHADER_STORAGE_BUFFER, GL_READ_ONLY);

    int i = 0;
    do
    {

        printf("iteration: %d ", i);
        glUnmapBuffer(GL_SHADER_STORAGE_BUFFER);
        cmd[CMD_BUF_RESPONSE] = INITIALIZED;


        switch (i % 4)
        {
            case 0:
                cmd[CMD_BUF_CMD] = CMD_THIN_N;
                break;
            case 2:
                cmd[CMD_BUF_CMD] = CMD_THIN_S;
                break;
            case 1:
                cmd[CMD_BUF_CMD] = CMD_THIN_E;
                break;
            case 3:
                cmd[CMD_BUF_CMD] = CMD_THIN_W;
                break;
        }

        glBufferData(GL_SHADER_STORAGE_BUFFER, sizeof(cmd), cmd, GL_DYNAMIC_DRAW);

        glDispatchCompute(width / 16, height / 16, 1);
        glMemoryBarrier(GL_SHADER_STORAGE_BARRIER_BIT);

       glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 0, buffers[IMG_1]);
       ptr = (uint32_t *)glMapBuffer(GL_SHADER_STORAGE_BUFFER, GL_READ_ONLY);
       unsigned int localcount = 0;
            for (int j=0; j < width * height; j++)
            {
                if (ptr[j] == 1)
                {
                    localcount++;
                }
            }
            printf("count: %u\n", localcount);

       memcpy(data, ptr, sizeof(uint32_t) * width * height);
       glUnmapBuffer(GL_SHADER_STORAGE_BUFFER);

       glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 1, buffers[IMG_0]);
       glBufferData(GL_SHADER_STORAGE_BUFFER, sizeof(uint32_t) * width * height, data, GL_DYNAMIC_DRAW);


       /*
        if (i % 2 == 0)
        {
            glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 0, buffers[IMG_1]);
            ptr = (uint32_t *)glMapBuffer(GL_SHADER_STORAGE_BUFFER, GL_READ_ONLY);
            unsigned int localcount = 0;
            for (int j=0; j < width * height; j++)
            {
                if (ptr[j] == 1)
                {
                    localcount++;
                }
            }
            printf("count: %u\n", localcount);
            glUnmapBuffer(GL_SHADER_STORAGE_BUFFER);

            printf("Input is now img_1. Output is img_0\n");
            glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 1, buffers[IMG_1]);
            glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 2, buffers[IMG_0]);
            checkError(__LINE__);
        }
        else
        {
            glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 0, buffers[IMG_0]);
            ptr = (uint32_t *)glMapBuffer(GL_SHADER_STORAGE_BUFFER, GL_READ_ONLY);
            unsigned int localcount = 0;
            for (int j=0; j < width * height; j++)
            {
                if (ptr[j] == 1)
                {
                    localcount++;
                }
            }
            printf("count: %u\n", localcount);
            glUnmapBuffer(GL_SHADER_STORAGE_BUFFER);



            printf("Input is now img_0. Output is img_1\n");
            glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 1, buffers[IMG_0]);
            glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 2, buffers[IMG_1]);
            checkError(__LINE__);
        }
        */

        glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 0, buffers[CMD]);
        ptr = (uint32_t *)glMapBuffer(GL_SHADER_STORAGE_BUFFER, GL_READ_ONLY);
        printf("cmd issued at start: %d response: %d\n",  ptr[CMD_BUF_CMD], ptr[CMD_BUF_RESPONSE]);
        i++;
        printf("\n");
    } while(ptr[CMD_BUF_RESPONSE] != INITIALIZED && i < 1000);

    glUnmapBuffer(GL_SHADER_STORAGE_BUFFER); // Free ptr


    // Transform Binary image (0, 1) to (0, 0xFFFFFFFF) values for texture display
    cmd[CMD_BUF_CMD] = CMD_REGULARIZE;
    glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 0, buffers[CMD]);
    glBufferData(GL_SHADER_STORAGE_BUFFER, sizeof(cmd), cmd, GL_DYNAMIC_DRAW);

    glDispatchCompute(width / 16, height / 16, 1);
    glMemoryBarrier(GL_SHADER_STORAGE_BARRIER_BIT);

    glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 0, buffers[CMD]);
    ptr = (uint32_t *)glMapBuffer(GL_SHADER_STORAGE_BUFFER, GL_READ_ONLY);
    printf("Regularize: cmd: %d  width: %d  height: %d response: %d\n",  ptr[CMD_BUF_CMD], ptr[CMD_BUF_WIDTH], ptr[CMD_BUF_HEIGHT], ptr[CMD_BUF_RESPONSE]);


    // Create texure
    glUnmapBuffer(GL_SHADER_STORAGE_BUFFER);


    glGenTextures(2, textures);
    checkError(__LINE__);
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, textures[0]);
        checkError(__LINE__);


    /*
    TEST Code for checking. TODO remove....
        glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 2, buffers[IMG_1]);
        ptr = (uint32_t *)glMapBuffer(GL_SHADER_STORAGE_BUFFER, GL_READ_ONLY);
        for (i = 0; i < width * height; i++)
        {
            if (ptr[i] == 0 && data[i] != 0)
            {
                printf("Found a spot. Value: %d\n", ptr[i]);
            }
        }

        return;
    */


    if (i % 2 == 0)
    {
        printf("output image is img_1\n");
        glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 2, buffers[IMG_1]);
        ptr = (uint32_t *)glMapBuffer(GL_SHADER_STORAGE_BUFFER, GL_READ_ONLY);

    }
    else
    {
        printf("output image is img_0\n");
        glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 2, buffers[IMG_0]);
        ptr = (uint32_t *)glMapBuffer(GL_SHADER_STORAGE_BUFFER, GL_READ_ONLY);

    }


    // Is there any difference between compute shader data and original?
    /*
    for (i = 0; i < width * height; i++)
    {
        if (ptr[i] == 0 && data[i] != 0)
        {
            printf("Found a spot. pos: %d\n", i);
        }
    }
    */
    glUnmapBuffer(GL_SHADER_STORAGE_BUFFER);



    glUseProgram(shaderProgram);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0,  GL_RED,  GL_UNSIGNED_INT, ptr);  //TODO: this is wrong. worry about later.
        checkError(__LINE__);

    glUniform1i(glGetUniformLocation(shaderProgram, "texData"), 0);
    checkError(__LINE__);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);





    checkError(__LINE__);
}

void initGL()
{
    // Vertices & texture init

    GLuint vao;
    glGenVertexArrays(1, &vao);
    glBindVertexArray(vao);

    GLuint vbo;
    glGenBuffers(1, &vbo);

    GLfloat vertices[] = {
        // X    Y      S    T
        -1.0f,  1.0f, 0.0f, 0.0f, // Top-left
         1.0f,  1.0f, 1.0f, 0.0f, // Top-right
         1.0f, -1.0f, 1.0f, 1.0f, // Bottom-right
        -1.0f, -1.0f, 0.0f, 1.0f  // Bottom-left
    };

    glBindBuffer(GL_ARRAY_BUFFER, vbo);
    glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);

    GLuint ebo;
    glGenBuffers(1, &ebo);

    GLuint elements[] = {
        0, 1, 2,
        2, 3, 0
    };

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ebo);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(elements), elements, GL_STATIC_DRAW);

    // Create shaders

    GLuint vertexShader = glCreateShader(GL_VERTEX_SHADER);
    glShaderSource(vertexShader, 1, &vertexSource, NULL);
    glCompileShader(vertexShader);

    GLuint fragmentShader = glCreateShader(GL_FRAGMENT_SHADER);
    glShaderSource(fragmentShader, 1, &fragmentSource, NULL);
    glCompileShader(fragmentShader);

    shaderProgram = glCreateProgram();
    glAttachShader(shaderProgram, vertexShader);
    glAttachShader(shaderProgram, fragmentShader);
    glBindFragDataLocation(shaderProgram, 0, "outColor");
    glLinkProgram(shaderProgram);
    glUseProgram(shaderProgram);

    // Vertex data specification
    GLint posAttrib = glGetAttribLocation(shaderProgram, "position");
    glEnableVertexAttribArray(posAttrib);
    glVertexAttribPointer(posAttrib, 2, GL_FLOAT, GL_FALSE, 4 * sizeof(GLfloat), 0);

    GLint texAttrib = glGetAttribLocation(shaderProgram, "texcoord");
    glEnableVertexAttribArray(texAttrib);
    glVertexAttribPointer(texAttrib, 2, GL_FLOAT, GL_FALSE, 4 * sizeof(GLfloat), (void *)(2 * sizeof(GLfloat)));
    checkError(__LINE__);



}

int main(int argc, char** argv)
{
    // Image setup

    PGM pgmImage;
    pgmImage.ReadFile("test.pgm");

    uint32_t *data = new uint32_t[pgmImage.GetHeight() * pgmImage.GetWidth()];
    uint32_t *data2 = new uint32_t[pgmImage.GetHeight() * pgmImage.GetWidth()];
    unsigned int size = pgmImage.GetHeight() * pgmImage.GetWidth();
    uint8_t *pgmData = pgmImage.GetData();
    for (int i=0; i < size; i++)
    {
        data[i] = pgmData[i];
        data2[i] = pgmData[i];
    }

    int count = 0;
    for (int i =0; i < pgmImage.GetHeight() * pgmImage.GetWidth(); i++)
    {
        if (data[i] == 0xFF)
        {
            count++;
        }
    }

    printf("count: %d\n", count);

    // Window Setup

    glutInitWindowSize(640, 400);
    glutInitWindowPosition (140, 140);
    glutInitDisplayMode(GLUT_RGB | GLUT_DOUBLE);
    glutInit(&argc, argv);

    glutCreateWindow( "OpenGL Application" );
    glutDisplayFunc(display);
    glutReshapeFunc(reshape);

    glewExperimental = true; // Needed for core profile
    if (glewInit() != GLEW_OK) {
        fprintf(stderr, "Failed to initialize GLEW\n");
        return -1;
    }

    initGL();
    runComputeProgram(data, data2);
    checkError(__LINE__);
    glutMainLoop();
    return 0;


}

