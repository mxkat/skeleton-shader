// Include standard headers
#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <math.h>

// Include GLEW
#include <GL/glew.h>

//Glut
#include <GL/glut.h>

//Project specific
#include "skeletonize.hpp"
#include "PGM.hpp"

// OpenGL shader info
GLuint programID;
GLuint output_image;

const GLchar* computeSource =
    "#version 450 core\n"
    "\n"
    "layout (local_size_x = 1) in;\n"
    "layout (std430, binding = 0) buffer Pos {\n"
    "uint values[];"
    "};"
    "layout (std430, binding = 1) buffer foo { uint bar;};"
    "\n"
    "layout (std430, binding = 2) buffer Expanded {\n"
    "uint evalues[];"
    "};"
    "\n"
    "void expand() {\n"
    "       evalues[gl_GlobalInvocationID.x * 4    ] = values[gl_GlobalInvocationID.x] & 0xFF;\n"
    "       evalues[gl_GlobalInvocationID.x * 4 + 1] = (values[gl_GlobalInvocationID.x] >> 8) & 0xFF;\n"
    "       evalues[gl_GlobalInvocationID.x * 4 + 2] = (values[gl_GlobalInvocationID.x] >> 16) & 0xFF;\n"
    "       evalues[gl_GlobalInvocationID.x * 4 + 3] = (values[gl_GlobalInvocationID.x] >> 24) & 0xFF;\n"
    "}\n"
    "void main(){"
    "    if (bar == 1 ) {                       \n"
    "       expand();                           \n"
    "       bar = bar + 1;                      \n"
    "    }                                      \n"
    "    if (bar == 2) {                        \n"
    "       evalues[gl_GlobalInvocationID.x] = evalues[gl_GlobalInvocationID.x] + 1;    \n"
    "    }                                                                              \n"
    "}";

GLuint computeProgram, SSBO;
uint8_t nums[12] = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12 };
uint32_t enums[12];

// Shader sources
const GLchar* vertexSource =
    "#version 450 core\n"
    "in vec2 position;"
    "in vec2 texcoord;"
    "out vec2 Texcoord;"
    "void main()"
    "{"
    "    Texcoord = texcoord;"
    "    gl_Position = vec4(position, 0.0, 1.0);"
    "}";

const GLchar* fragmentSource =
    "#version 450 core\n"
    "in vec2 Texcoord;"
    "out vec4 outColor;"
    "uniform sampler2D texData;"
    "void main()"
    "{"
    "   vec4 imColor = texture(texData, Texcoord);"
    "   outColor = vec4(0.0, imColor.r, 0.0, 1.0);"
    //"    outColor = texture(texData, Texcoord);"
    //"    outColor = vec4(1.0, 1.0, 0.0, 1.0);"
    "}";


void checkError(int line)
{
    GLint err;

    do
    {
        err = glGetError();
        switch (err)
        {
            case GL_NO_ERROR:
                //printf("%d: No error\n", line);
                break;
            case GL_INVALID_ENUM:
                printf("%d: Invalid enum!\n", line);
                break;
            case GL_INVALID_VALUE:
                printf("%d: Invalid value\n", line);
                break;
            case GL_INVALID_OPERATION:
                printf("%d: Invalid operation\n", line);
                break;
            case GL_INVALID_FRAMEBUFFER_OPERATION:
                printf("%d: Invalid framebuffer operation\n", line);
                break;
            case GL_OUT_OF_MEMORY:
                printf("%d: Out of memory\n", line);
                break;
            default:
                printf("%d: glGetError default case. Should not happen!\n", line);
        }
    } while (err != GL_NO_ERROR);
}

void display()
{

	glClearColor(0.0f, 0.0f, 1.0f, 0.0f);

	glClear(GL_COLOR_BUFFER_BIT);

    glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0);

    glFlush();
    glutSwapBuffers();

}


void reshape(int width,int height)
{
    double w2h = (height>0) ? (double)width/height : 1;
    //  Set viewport as entire window
    glViewport(0,0, width,height);

}

void runComputeProgram()
{
    uint32_t *ptr;
    GLuint computeShader = glCreateShader(GL_COMPUTE_SHADER);
    glShaderSource(computeShader, 1, &computeSource, NULL);
    glCompileShader(computeShader);


    computeProgram = glCreateProgram();
    glAttachShader(computeProgram, computeShader);
    glLinkProgram(computeProgram);
    GLint status;
    glGetProgramiv(computeProgram, GL_LINK_STATUS, &status);

    if (status == GL_TRUE)
    {
        printf("link good\n");
    }
    else
    {
        printf("link bad\n");
        GLchar log[4096];
        GLsizei len;

        glGetProgramInfoLog(computeProgram, 4096, &len, log);

        printf("%s\n", log);

    }


    glGenBuffers(1, &SSBO);
    glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 0, SSBO);
    glBufferData(GL_SHADER_STORAGE_BUFFER, sizeof(nums), nums, GL_DYNAMIC_DRAW);



    GLuint SSBOaction;
    uint action = 1;
    glGenBuffers(1, &SSBOaction);
    glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 1, SSBOaction);
    glBufferData(GL_SHADER_STORAGE_BUFFER, sizeof(action), &action, GL_DYNAMIC_DRAW);

    GLuint evals;
    glGenBuffers(1, &evals);
    glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 2, evals);
    glBufferData(GL_SHADER_STORAGE_BUFFER, sizeof(enums), &enums, GL_DYNAMIC_DRAW);

    glUseProgram(computeProgram);


    glDispatchCompute(12 / sizeof(uint32_t), 1, 1);
    glMemoryBarrier(GL_SHADER_STORAGE_BARRIER_BIT);

    glDispatchCompute(12, 1, 1);
    glDispatchCompute(12, 1, 1);
    glMemoryBarrier(GL_SHADER_STORAGE_BARRIER_BIT);

    //glDispatchCompute(10, 1, 1);
    //glMemoryBarrier(GL_SHADER_STORAGE_BARRIER_BIT);
    glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 2, evals);
    ptr = (uint32_t *)glMapBuffer(GL_SHADER_STORAGE_BUFFER, GL_READ_ONLY);


    for (int i=0; i < 12; i++)
    {
        printf("%d ", ptr[i]);
    }
    printf("\n");

    /*
    //Uncomment next line to re-bind action to the GPU. results will be action = 1
    //glBufferData(GL_SHADER_STORAGE_BUFFER, sizeof(action), &action, GL_DYNAMIC_DRAW);
    glDispatchCompute(10, 1, 1);
    glMemoryBarrier(GL_SHADER_STORAGE_BARRIER_BIT);

    glUnmapBuffer(GL_SHADER_STORAGE_BUFFER);
    ptr = (uint32_t *)glMapBuffer(GL_SHADER_STORAGE_BUFFER, GL_WRITE_ONLY);

    while (*ptr < 10)
    {
        glDispatchCompute(10, 1, 1);
        glMemoryBarrier(GL_SHADER_STORAGE_BARRIER_BIT);
        printf("action: %u\n", *ptr);
        glUnmapBuffer(GL_SHADER_STORAGE_BUFFER);
        ptr = (uint32_t *)glMapBuffer(GL_SHADER_STORAGE_BUFFER, GL_WRITE_ONLY);
    }

    glUnmapBuffer(GL_SHADER_STORAGE_BUFFER);
     checkError(__LINE__);
    glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 0, SSBO);
     checkError(__LINE__);
    ptr = (uint32_t *)glMapBuffer(GL_SHADER_STORAGE_BUFFER, GL_WRITE_ONLY);

    printf("Original buffer\n");
    for (int i=0; i < 10; i++)
    {
        printf("%u ", nums[i]);
    }
    printf("\n");

    printf("Computed buffer\n");
    for (int i=0; i < 10; i++)
    {
        printf("%u ", ptr[i]);
    }
    printf("\n");



    glUnmapBuffer(GL_SHADER_STORAGE_BUFFER);
    checkError(__LINE__);
    */
}

int main(int argc, char** argv)
{
    // Image setup

    PGM pgmImage;
    pgmImage.ReadFile("test.pgm");

    // Window Setup

    glutInitWindowSize(640, 400);
    glutInitWindowPosition (140, 140);
    glutInitDisplayMode(GLUT_RGB | GLUT_DOUBLE);
    glutInit(&argc, argv);

    glutCreateWindow( "OpenGL Application" );
    glutDisplayFunc(display);
    glutReshapeFunc(reshape);

    glewExperimental = true; // Needed for core profile
    if (glewInit() != GLEW_OK) {
        fprintf(stderr, "Failed to initialize GLEW\n");
        return -1;
    }

    runComputeProgram();
    checkError(__LINE__);
    return 0;


}

