#include "PGM.hpp"

#define PGM_HEADER "P5"

PGM::PGM()
{
    mpData = NULL;
    Clear();
}

PGM::~PGM()
{
    Clear();
}

uint8_t* PGM::GetData()
{
    return mpImgData;
}


uint16_t PGM::GetWidth()
{
    return mWidth;
}

uint16_t PGM::GetHeight()
{
    return mHeight;
}

uint8_t PGM::GetMaxWhite()
{
    return mMaxWhite;
}

void PGM::Clear()
{
    if (mpData != NULL)
    {
        delete[] mpData;
    }
    
    mpImgData = NULL;
    mWidth = 0;
    mHeight = 0;
    mMaxWhite = 255;
}

// Finds the 
int PGM::PopulateFields(size_t size)
{
    int i;
    bool EOL = false;
    bool haveWhite = false;
    bool comment = false;
    
    if (mpData == NULL) { return -1; }

    // Check header
    if ((mpData[0] != 0x50) || (mpData[1] != 0x35)) { return -2; }
    
    //Ignore the comment
    //Start at 3rd position in file, after "P5" header    
    for (i = 2; i < size; i++)
    {
        if (mpData[i] == '#') 
        { 
            comment = true;
            continue;
        }
        
        if (mpData[i] == 0x0A && comment == true)
        {
            comment = false;
            break;
        }
        
        if (comment == true) 
        { 
            continue; 
        }
        
    }
    
    // Get width and height
    i++;
    sscanf((char *)&mpData[i], "%4" SCNu16 " %4" SCNu16, &mWidth, &mHeight);
    
    for (i; i < size; i++)
    {
        //Move past the width and height we just found
        if (mpData[i] == 0x0A && EOL == false)
        {
            EOL = true;
            continue;
        }
        
        // If past the width and height, now at the range. Save it.
        if (EOL == true && haveWhite == false)
        {
            sscanf((char *)&mpData[i], "%3" SCNu8, &mMaxWhite);
            haveWhite = true;
        }
        
        if (haveWhite == true && mpData[i] == 0x0A)
        {
            i++; //Move to next element - start of the actual data
            break;
        }
    }
    
    if (i == size)
    {
        return -3; //Did not find the start of data.
    }
     
    mpImgData = &mpData[i];
    
    return 0;
}

// Reads a PGM file. Returns 0 on success, other values on failure
int PGM::ReadFile(const char *pPath)
{
    struct stat st;
    int fd;
 
    if (this->mpData != NULL)
    {
        Clear();
    }
       
    if (stat(pPath, &st) != 0)
    {
        return 1;
    }
    
    fd = open(pPath, O_RDONLY);
    
    if (fd == -1)
    {
        return 1;
    }
    
    //this->mpData = (uint8_t *) malloc(st.st_size);
    mpData = new uint8_t[st.st_size];
    
    if (this->mpData == NULL)
    {
        return 2;
    }
    
    if (read(fd, this->mpData, st.st_size) == -1)
    {
        Clear();
    }

    close(fd);
        
    PopulateFields(st.st_size);
    
    return 0;
}

