#ifndef __SKELETON_H__
#define __SKELETON_H__

#include <stdint.h>

void GaussianBlur(uint8_t *data, uint16_t width, uint16_t height);

void Skeleton(uint8_t *data, uint16_t width, uint16_t height);
void Skeleton2(uint8_t *data, uint16_t width, uint16_t height);
void Skeleton3(uint8_t *data, uint16_t width, uint16_t height);
void Skeleton4(uint8_t *data, uint16_t width, uint16_t height);
#endif // __SKELETON_H__
