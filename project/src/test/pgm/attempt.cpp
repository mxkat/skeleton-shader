// Include standard headers
#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <math.h>

// Include GLEW
#include <GL/glew.h>

//Glut
#include <GL/glut.h>

// Include GLM
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
using namespace glm;

#include "shader.hpp"
#include "PGM.hpp"

GLuint programID;
GLuint output_image;

void display()
{
    glBindImageTexture(1, output_image, 0, GL_FALSE, 0, GL_WRITE_ONLY, GL_R8UI);
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, output_image);

	glClearColor(0.0f, 0.0f, 1.0f, 0.0f);
    glUseProgram(programID);
    glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
    
    glFlush();
    glutSwapBuffers();
    
}    
    
    
void reshape(int width,int height)
{
    double w2h = (height>0) ? (double)width/height : 1;
    //  Set viewport as entire window
    glViewport(0,0, width,height);
    
    /*
    //  Select projection matrix
    glMatrixMode(GL_PROJECTION);
    //  Set projection to identity
    glLoadIdentity();
    //  Orthogonal projection
    glOrtho(-w2h, w2h, -2.0, +2.0, -2.0, +2.0);
    //  Select model view matrix
    glMatrixMode(GL_MODELVIEW);
    //  Set model view to identity
    glLoadIdentity();
    */
}



int main(int argc, char** argv)
{
    PGM pgmImage;
    
    pgmImage.ReadFile("test.pgm");
    
    glutInitWindowSize(640, 400);
    glutInitWindowPosition (140, 140);
    glutInitDisplayMode(GLUT_RGB | GLUT_DOUBLE);
    glutInit(&argc, argv);

    glutCreateWindow( "OpenGL Application" );
    glutDisplayFunc(display);
    glutReshapeFunc(reshape);

	
    glewExperimental = true; // Needed for core profile
    if (glewInit() != GLEW_OK) {
        fprintf(stderr, "Failed to initialize GLEW\n");
        return -1;
    }
	
	
    // Create and compile our GLSL program from the shaders
	programID = LoadShaders( "basic.vertexshader", "basic.fragmentshader" );

	// Get a handle for our "MVP" uniform
	GLuint MatrixID = glGetUniformLocation(programID, "MVP");
	
	GLuint  render_vao;
    GLuint  render_vbo;
    
	glGenVertexArrays(1, &render_vao);
    glBindVertexArray(render_vao);
    glEnableVertexAttribArray(0);
    glGenBuffers(1, &render_vbo);
    glBindBuffer(GL_ARRAY_BUFFER, render_vbo);
    static const float verts[] =
    {
        -1.0f, -1.0f, 0.5f, 1.0f,
         1.0f, -1.0f, 0.5f, 1.0f,
         1.0f,  1.0f, 0.5f, 1.0f,
        -1.0f,  1.0f, 0.5f, 1.0f,
    };
    glBufferData(GL_ARRAY_BUFFER, sizeof(verts), verts, GL_STATIC_DRAW);
    glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, 0, NULL);

    glGenTextures(1, &output_image);
    glBindTexture(GL_TEXTURE_2D, output_image);
    glTexStorage2D(GL_TEXTURE_2D, 0, GL_R8UI, pgmImage.GetWidth(), pgmImage.GetHeight());
    glTexImage2D(GL_TEXTURE_2D, 0, GL_R8UI, pgmImage.GetWidth(), pgmImage.GetHeight(), 0, GL_RED, GL_UNSIGNED_BYTE, pgmImage.GetData());
    
    glutMainLoop();
    
    return 0;
}
    
