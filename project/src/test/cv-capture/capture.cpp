#include "opencv2/opencv.hpp"

#include <stdio.h>

using namespace cv;

int main(int, char**)
{
    VideoCapture cap(0); // open the default camera
    if(!cap.isOpened())  // check if we succeeded
        return -1;
    cap.set(CV_CAP_PROP_FRAME_WIDTH,640);
    cap.set(CV_CAP_PROP_FRAME_HEIGHT,480);

    //cap.set(CV_CAP_PROP_FPS, 15);

    Mat edges;
    namedWindow("edges",1);

    bool flag = true;
    for(;;)
    {
        Mat frame;
        cap >> frame; // get a new frame from camera
        //cvtColor(frame, edges, CV_BGR2GRAY);
        edges = frame; //If want color, uncomment this and comment the line above

        if (flag == true)
        {
            std::cout << edges.elemSize1() << std::endl;
            std::cout << edges.channels() << std::endl;
            int res = edges.depth();
            printf("Type: ");
            switch (res)
            {
                case CV_8U:
                    printf("CV_8U\n");
                    break;
                case CV_8S:
                    printf("CV_8S\n");
                    break;
                case CV_16U:
                    printf("CV_16U\n");
                    break;
                case CV_16S:
                    printf("CV_16S\n");
                    break;
                case CV_32S:
                    printf("CV_32S\n");
                    break;
                case CV_32F:
                    printf("CV_32F\n");
                    break;
                case CV_64F:
                    printf("CV_64F\n");
                    break;
            }
            flag = false;
        }

        //GaussianBlur(edges, edges, Size(7,7), 1.5, 1.5);
        //Canny(edges, edges, 0, 30, 3);
        imshow("edges", edges);
        if(waitKey(30) >= 0) break;
    }
    // the camera will be deinitialized automatically in VideoCapture destructor
    return 0;
}
